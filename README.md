# Semantic Versioning - GitLab CI

[![pipeline status](https://gitlab.com/dime.nicius/semver_ci/badges/master/pipeline.svg)](https://gitlab.com/dime.nicius/semver_ci/commits/master)  [![coverage report](https://gitlab.com/dime.nicius/semver_ci/badges/master/coverage.svg)](https://gitlab.com/dime.nicius/semver_ci/-/commits/master)

Esse é um exemplo de pipeline de Golang usando versionamento automático de código na branch `master` e criação de imagens Docker com versão correspondente no `registry` do projeto.

## Configuração

Para que o pipeline rode completamente é necessário que se configure uma `token` de acesso a API no projeto. Nesse caso foi gerado uma `token` em minha conta pessoal, mas em projetos corporativos o ideal é que se crie uma *conta de serviço* e gere o `token` nela.

Adicione como variáveis do projeto (**Settings > CI/CD > Variables**), os valores:

- **NPA_USERNAME** - nome do usuário
- **NPA_PASSWORD** - token de acesso a api

Crie e adicione as seguintes `labels` no projeto (**Issues > Labels**):

- **bumb-major**
- **bump-minor**


Essas `labels` são utilizadas nos `merge requests` para definir qual será o tipo de atualização feito pelo versionamento. Caso não seja utilizada nenhuma `tag`, o script fará o `bump` na versão de **PATCH**, no caso de uma das `labels` ser definidas o `bump` acontece na versão **MAJOR** ou **MINOR** respectivamente.

## Pipeline

A criação do pipeline foi baseada no repositório: [gitlab-semantic-versioning (mrooding)](https://github.com/mrooding/gitlab-semantic-versioning)


O pipeline é composto dos `stages`:
  - env-vars
  - test
  - build
  - tag-version
  - registry


#### env-vars
Responsável por gerar as variáveis utilizadas pelos `jobs` de versionamento e registry.

#### test
Job que executa os tests de Golang.

#### build
Job que realiza o build da imagem Docker e verifica se não ocorrem erros antes que a `tag` seja criada.

#### tag-version
Execução do script de versionamento, para mais informações sobre como o script funciona ver o [repositório de origem](https://github.com/mrooding/gitlab-semantic-versioning).

#### registry
Build da imagem Docker final, `push` para o repositório com a `tag`gerada e atualização da imagem com `tag` *latest*.
